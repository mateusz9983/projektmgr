#include "opencv2/opencv.hpp"
#include "iostream"
#include <list>
#include <math.h>
#include <omp.h>
using namespace cv;
using namespace std;

struct sortClassY
{
    bool operator()(Point pt1, Point pt2)
    {
        return (pt1.y < pt2.y);
    }
} sortObjectY;
struct sortClassX
{
    bool operator()(Point pt1, Point pt2)
    {
        return (pt1.x < pt2.x);
    }
} sortObjectX;

struct object
{
    vector<Point2f> points[2];
    int maxWidth, startXRectangle, maxHeight, startYRectangle;
    Point pointFirstRectangle;
    Point pointSecondRectangle;
    bool isSorted;
};

Mat image, input, imageCopy, gray;

int getPixelIntensityValue(int x, int y)
{
    Scalar scalar;
    scalar = gray.at<uchar>(y, x);
    return scalar.val[0];
}

bool isCorrectIntensity(int pixelIntensity, int min, int max)
{
    if ((pixelIntensity < min) || (pixelIntensity > max))
        return true;
    return false;
}

bool checkShapePixels(int x, int y, int min, int max)
{

    int listOfPixels[12];
    listOfPixels[0] = getPixelIntensityValue(x + 1, y + 3);

    listOfPixels[1] = getPixelIntensityValue(x + 2, y + 2);

    listOfPixels[2] = getPixelIntensityValue(x + 3, y + 1);

    listOfPixels[3] = getPixelIntensityValue(x + 3, y - 1);

    listOfPixels[4] = getPixelIntensityValue(x + 2, y - 2);

    listOfPixels[5] = getPixelIntensityValue(x + 1, y - 3);

    listOfPixels[6] = getPixelIntensityValue(x - 1, y - 3);

    listOfPixels[7] = getPixelIntensityValue(x - 2, y - 2);

    listOfPixels[8] = getPixelIntensityValue(x - 3, y + 1);

    listOfPixels[9] = getPixelIntensityValue(x - 3, y + 1);

    listOfPixels[10] = getPixelIntensityValue(x - 2, y + 2);

    listOfPixels[11] = getPixelIntensityValue(x - 1, y + 3);

    for (int i = 0; i < 12; i++)
        if (!isCorrectIntensity(listOfPixels[i], min, max))
            return false;
    return true;
}

void drawPoint(Mat &image, vector<Point2f> vectorOfFeature, int name)
{
    for (int i = 0; i < vectorOfFeature.size(); i++)
    {
        circle(image, vectorOfFeature[i], 4, Scalar(0, 0, 255), 1);
    }
    imwrite(to_string(name) + ".jpg", image);
}

vector<Point2f> getFeatures(Rect2d r, bool draw = false, int minCountPoints = 30, int precision = 10)
{
    cvtColor(input, image, COLOR_BGR2GRAY);
    vector<Point2f> vectorOfFeature;

    for (int x = r.x; x < r.x + input.cols; x++)
        for (int y = r.y; y < r.y + input.rows; y++)
        {
            int pixelIntensity = getPixelIntensityValue(x, y);
            int min = int(pixelIntensity * ((100.0 - double(precision)) / 100.0));
            int max = int(pixelIntensity * ((100.0 + double(precision)) / 100.0));

            int pixel1 = getPixelIntensityValue(x, y + 3);
            int pixel5 = getPixelIntensityValue(x + 3, y);
            int pixel9 = getPixelIntensityValue(x, y - 3);
            int pixel13 = getPixelIntensityValue(x - 3, y);

            if (isCorrectIntensity(pixel1, min, max) &&
                isCorrectIntensity(pixel5, min, max) &&
                isCorrectIntensity(pixel9, min, max) &&
                isCorrectIntensity(pixel13, min, max))
                if (checkShapePixels(x, y, min, max))
                {
                    Point2f pt;
                    pt.x = x;
                    pt.y = y;
                    vectorOfFeature.push_back(pt);
                }
        }
    if (minCountPoints > vectorOfFeature.size())
    {
        vectorOfFeature.clear();
        cout << "Za mała liczba punktów. Wyznacz nowy obszar." << endl;
    }
    if (draw)
        drawPoint(gray, vectorOfFeature, rand());
    return vectorOfFeature;
}

vector<Point2f> computePoint(vector<Point2f> vectorOfFeature, int maxSize)
{
    vector<Point2f> copyVectorOfFeature = vectorOfFeature;
    Point2f centerPoint = vectorOfFeature[vectorOfFeature.size() / 2];
    int counter = 0;
    vector<Point2f>::iterator itTMP;
    for (int i = 0; i < copyVectorOfFeature.size(); i++)
    {
        if (sqrt(pow((copyVectorOfFeature[i].x - centerPoint.x), 2) + pow((copyVectorOfFeature[i].y - centerPoint.y), 2)) > maxSize)
        {
            itTMP = vectorOfFeature.begin() + i - counter;
            vectorOfFeature.erase(itTMP);
            counter++;
        }
    }
    return vectorOfFeature;
}

int main(int argc, char **argv)
{
    int maxWidth, maxRangeWidth, startXRectangle;
    int maxHeight, maxRangeHeight, startYRectangle;
    TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.03);
    Size subPixWinSize(10, 10), winSize(31, 31);
    const int MAX_COUNT = 500;
    bool wantSelect = false, live = false;
    int minCountPoints = 1;
    int precision = 30;
    Mat prevGray, frame, tmpImage;
    int frameCounter = 0;
    int tick = 0;
    int fps;
    VideoCapture cap;
    if (live)
    {
        cv::CommandLineParser parser(argc, argv, "{@input|0|}");
        string inputVideo = parser.get<string>("@input");
        if (inputVideo.size() == 1 && isdigit(inputVideo[0]))
            cap.open(inputVideo[0] - '0');
        else
            cap.open(inputVideo);

        if (!cap.isOpened())
        {
            cout << "Nie udało się przechwycić viedo z urządzenia...\n";
            return 0;
        }
    }
    else
        cap.open("mecz.mp4");

    std::time_t timeBegin = std::time(0);
    vector<Rect2d> rects;
    vector<Point2f> points[2];
    vector<object> vectorOfObject;
    char c;

    for (;;)
    {
        cap >> frame;

        if (frame.empty())
            break;
        frame.copyTo(imageCopy);
        cvtColor(frame, gray, COLOR_BGR2GRAY);

        if (wantSelect)
        {
            Rect2d r;
            r = selectROI(frame);
            startXRectangle = r.x;
            startYRectangle = r.y;
            maxWidth = r.width;
            maxHeight = r.height;
            cout << maxWidth << "x" << maxHeight << " " << endl;
            wantSelect = false;
            input = frame(r);
            // funkcja wyznaczania dobrych cech do śledzenia z biblioteki OpenCV
            // Wykorzystywana będzie przy porównaniu algorytmów
            /*cvtColor(input, image, COLOR_BGR2GRAY);
            goodFeaturesToTrack(image, points[1], 9999999, 0.01, 0,3, Mat(), 3, false, 0.04);
            Point2f tmpPoint;
            for(int i=0; i<points[1].size();i++){
                tmpPoint.x= points[1][i].x+startXRectangle;
                tmpPoint.y = points[1][i].y+startYRectangle;
                points[1][i]=tmpPoint;

            }
            */
            Point pointFirstRectangle;
            pointFirstRectangle.x = 0;
            pointFirstRectangle.y = 0;
            Point pointSecondRectangle;
            pointSecondRectangle.x = 0;
            pointSecondRectangle.y = 0;

            points[1] = getFeatures(r, true, minCountPoints, precision);
            if (points[1].size() == 0)
                continue;

            object obj;
            obj.startXRectangle = startXRectangle;
            obj.startYRectangle = startYRectangle;
            obj.maxWidth = maxWidth;
            obj.maxHeight = maxHeight;
            obj.points[1] = points[1];
            obj.points[0] = points[0];
            obj.pointFirstRectangle = pointFirstRectangle;
            obj.pointSecondRectangle = pointSecondRectangle;
            obj.isSorted = false;
            vectorOfObject.push_back(obj);
        }
#pragma omp parallel for shared(vectorOfObject) num_threads(4)
        for (int i = 0; i < vectorOfObject.size(); i++)
        {
            if (!vectorOfObject[i].points[0].empty())
            {

                vector<uchar> status;
                vector<float> err;
                if (prevGray.empty())
                    gray.copyTo(prevGray);
                calcOpticalFlowPyrLK(prevGray, gray, vectorOfObject[i].points[0], vectorOfObject[i].points[1], status, err, winSize, 3, termcrit, 0, 0.001);
                maxRangeHeight = vectorOfObject[i].maxHeight + float(vectorOfObject[i].maxHeight); // * 0.5;
                maxRangeWidth = vectorOfObject[i].maxWidth + float(vectorOfObject[i].maxWidth);    // * 0.5;

                Size s = imageCopy.size();
                if (vectorOfObject[i].points[1][0].x > s.width || vectorOfObject[i].points[1][0].y > s.height || vectorOfObject[i].points[1][0].x < 0 || vectorOfObject[i].points[1][0].y < 0)
                {
                    cout << "Obiekt poza obszarem." << endl;
#pragma omp critical
                    vectorOfObject.erase(vectorOfObject.begin() + i);
                    continue;
                }

                if (vectorOfObject[i].pointFirstRectangle.x == 0 && vectorOfObject[i].pointFirstRectangle.y == 0)
                {
                    vectorOfObject[i].pointFirstRectangle.x = vectorOfObject[i].startXRectangle;
                    vectorOfObject[i].pointFirstRectangle.y = vectorOfObject[i].startYRectangle;
                    vectorOfObject[i].pointSecondRectangle.x = vectorOfObject[i].startXRectangle;
                    vectorOfObject[i].pointSecondRectangle.y = vectorOfObject[i].startYRectangle;
                }

                if (!vectorOfObject[i].isSorted)
                {
                    sort(vectorOfObject[i].points[1].begin(), vectorOfObject[i].points[1].end(), sortObjectY);
                    vectorOfObject[i].pointFirstRectangle = vectorOfObject[i].points[1][0];
                }
                vectorOfObject[i].isSorted = false;

                if (abs(vectorOfObject[i].pointSecondRectangle.x - vectorOfObject[i].pointFirstRectangle.x) > maxRangeWidth)
                {
                    sort(vectorOfObject[i].points[1].begin(), vectorOfObject[i].points[1].end(), sortObjectX);
                    vectorOfObject[i].points[1] = computePoint(vectorOfObject[i].points[1], vectorOfObject[i].maxWidth);
                    if (minCountPoints > vectorOfObject[i].points[1].size())
                    {
                        cout << "Za mala liczba punktow" << endl;
#pragma omp critical
                        vectorOfObject.erase(vectorOfObject.begin() + i);
                        continue;
                    }
                    else
                        vectorOfObject[i].pointFirstRectangle.x = vectorOfObject[i].points[1][0].x;
                    vectorOfObject[i].isSorted = true;
                }

                if (abs(vectorOfObject[i].pointSecondRectangle.y - vectorOfObject[i].pointFirstRectangle.y) > maxRangeHeight)
                {
                    sort(points[1].begin(), points[1].end(), sortObjectY);
                    vectorOfObject[i].points[1] = computePoint(vectorOfObject[i].points[1], vectorOfObject[i].maxHeight);
                    if (minCountPoints > vectorOfObject[i].points[1].size())
                    {
                        cout << "Za mala liczba punktow" << endl;
#pragma omp critical
                        vectorOfObject.erase(vectorOfObject.begin() + i);
                        continue;
                    }
                    else
                        vectorOfObject[i].pointFirstRectangle.y = vectorOfObject[i].points[1][0].y;
                    vectorOfObject[i].isSorted = true;
                }
                putText(imageCopy, format("LP=%d", vectorOfObject[i].points[1].size()), Point(vectorOfObject[i].pointFirstRectangle.x, vectorOfObject[i].pointFirstRectangle.y - 5), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 255));
                vectorOfObject[i].pointSecondRectangle.x = vectorOfObject[i].pointFirstRectangle.x + vectorOfObject[i].maxWidth / 2;
                vectorOfObject[i].pointSecondRectangle.y = vectorOfObject[i].pointFirstRectangle.y + vectorOfObject[i].maxHeight / 2;
                rectangle(imageCopy, vectorOfObject[i].pointFirstRectangle, vectorOfObject[i].pointSecondRectangle, Scalar(0, 255, 0));
            }
            swap(vectorOfObject[i].points[1], vectorOfObject[i].points[0]);
        }

        swap(prevGray, gray);
        frameCounter++;
        time_t timeNow = time(0) - timeBegin;
        if (timeNow - tick >= 1)
        {
            tick++;
            fps = frameCounter;
            frameCounter = 0;
        }
        putText(imageCopy, format("Srednia liczba klatek=%d", fps), Point(30, 30), FONT_HERSHEY_SIMPLEX, 1, Scalar(0, 255, 255));
        putText(imageCopy, format("Liczba obiektow=%d", vectorOfObject.size()), Point(30, 45), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(255, 255, 255));
        imshow("Filmik", imageCopy);
        c = (char)waitKey(25);
        switch (c)
        {
        case 27:
            return 0;
            break;
        case 's':
            wantSelect = true;
            break;
        }
    }
    return 0;
}
